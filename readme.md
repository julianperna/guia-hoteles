## Sitio Reb Responsive
Utilizando Bootstrap y Flexbox, Grunt y diferente herramientas, node_js, sass,

1. Navbar
2. Carousel
3. Breadcrumb
4. Card
5. iconos usados open-iconic y fontaweson
6. Tabla-responsive
7. Formulario
8. Estilos independientes de bootstrap
9. Completamente responsive
10. server utilizado lite-server
11. Accordion
12. Tabs , Phills
13. Collapse Multi-Collapse
14. Tooltips
15. Modal
16. JavaScript para control del carousel, tooltips y popovers.

### Para usar clona el proyecto y ejecuta el comando

npm install

para que se instalen todas los modulos necesarios.